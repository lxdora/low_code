const path = require('path');

module.exports = {
  devServer: {
    open: true
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src/'),
        '@library': path.resolve(__dirname, 'src/library'),
        '@public': path.resolve(__dirname, 'public/'),
        '@properties': path.resolve(__dirname, 'public/properties')
      }
    }
  },
  productionSourceMap: false
};
