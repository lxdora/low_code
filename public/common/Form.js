const config = {
  "type": "form",
  "rule": [
    //单行文本
    {
      type: "input",
      title: "单行文本",  //label名称
      field: "field1", //字段名称
      value: "", //input值,
      props: {
        "type": "text", //输入框类型，可选值为 text、password、textarea、url、email、date
        "clearable": false, //是否显示清空按钮
        "disabled": false, //设置输入框为禁用状态
        "readonly": false, //设置输入框为只读
        "rows": 4, //文本域默认行数，仅在 textarea 类型下有效
        "autosize": false, //自适应内容高度，仅在 textarea 类型下有效，可传入对象，如 { minRows: 2, maxRows: 6 }
        "number": false, //将用户的输入转换为 Number 类型
        "autofocus": false, //自动获取焦点
        "autocomplete": "off", //原生的自动完成功能，可选值为 off 和 on
        "placeholder": "请输入单行文本", //占位文本
        "size": "default", //输入框尺寸，可选值为large、small、default或者不设置,
        "spellcheck": false, //原生的 spellcheck 属性
        "required": false, // 是否必填
      },
      validate: [{
        required: true,
        message: '请输入单行文本',
        trigger: 'blur'
      },
      ],
    },
    // 密码
    {
      type: "input",
      title: "密码",
      field: "password",
      value: "",
      props: {
        "type": "password",
        "clearable": false,
        "disabled": false,
        "readonly": false,
        "rows": 4, 
        "autosize": false,
        "number": false, 
        "autofocus": false,
        "autocomplete": "off",
        "placeholder": "至少包含两种以上字符，不少于8位",
        "size": "default",
        "spellcheck": false,
        "required": true
      },
      "validate": [
        {"required": true, "message": "必须填写密码", "trigger": "blur"},
        { pattern: /(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W_]).{6,18}/, message: '长度为6-18，包含大写、小写字母、数字和特殊字符（比如~!@#$%^&*()_+=><.,/)'}
      ]
    },
    // 多行文本框
    {
      type: "input",
      title: "多行文本框",
      field: "field2",
      value: "",
      props: {
        "type": "textarea",
        "placeholder": "请输入文本",
        "rows":5
      },
      validate: [{
        required: true,
        message: '请输入文本',
        trigger: 'blur'
      },
      ],
  },
    // 静态下拉框
    {
      type: "select",
      field: "select1",
      title: "静态下拉框",
      value: [],  // 下拉框的值
      options: [{    // 下拉框的选项, 可以写死
        "value": "1",
        "label": "管理员",
        "disabled": false
      },
      {
        "value": "2",
        "label": "录入人员",
        "disabled": false
      },
      {
        "value": "3",
        "label": "审核人员",
        "disabled": false
      }
      ],
      "props": {
        "placement": "bottom"
      }
    },
    // 动态下拉框
    {
      type: "select",
      field: "select2",
      title: "动态下拉框",
      value: [],  // 下拉框的值
      options: [
      ],
      "props": {
        "placement": "bottom"
      },
      effect:{   // 下拉框的选项从接口获取
        fetch: {
            action:'http://xiaokang.local.com/route.php?m=catalog&format=json',
            to: 'options',
            method:'GET',
            parse(res){
              const options = {}
              res.data.data.forEach(element => {
                if(element.parent_id){
                  options[element['parent_id_all']['id']] = element['parent_id_all']['name']
                }
              });
              const optionArr = []
              for(const key in options){
                optionArr.push({
                  label: options[key],
                  value: key
                })
              }
              return optionArr
            }
        }
      },
    },
    //级联选择
    {
      type: "cascader",
      title: "级联选择",
      field: "address",
      value: [],
      props: {
        data: [],   //可选项的数据源，格式参照https://cdn.jsdelivr.net/npm/form-create@1.4.4/district/province_city_area.js
        renderFormat: label => label.join(' / '), //选择后展示的函数，用于自定义显示格式
        disabled: false, //是否禁用选择器
        clearable: true, //是否支持清除
        placeholder: '请选择',  //输入框占位符
        trigger: 'click',  //次级菜单展开方式，可选值为 click 或 hover
        changeOnSelect: false,  //当此项为 true 时，点选每级菜单选项值都会发生变化
        size: undefined,  //输入框大小，可选值为large和small或者不填
        loadData: () => {},  //动态获取数据，数据源需标识 loading
        filterable: false,  //是否支持搜索
        notFoundText: '无匹配数据',  //当搜索列表为空时显示的内容
        transfer: false,  //是否将弹层放置于 body 内，在 Tabs、带有 fixed 的 Table 列内使用时，建议添加此属性，它将不受父级样式影响，从而达到更好的效果
      },
      validate: [],
    },
    //单选
    {
      type: "radio",
      title: "单选框", //label名称
      field: "radio", //字段名称
      value: 0, //input值,
      options: [{
        value: 0,
        label: "否",
        disabled: false
      },
      {
        value: 1,
        label: "是",
        disabled: false
      }
      ],
      props: {
        "type": undefined, //可选值为 button 或不填，为 button 时使用按钮样式
        "size": "default", //单选框的尺寸，可选值为 large、small、default 或者不设置
        "vertical": false, //是否垂直排列，按钮样式下无效
      },
    },
    // 多选
    {
      type: "checkbox",
      title: "多选框",
      field: "checkbox",
      value: ["1", "2", "3"], //input值,
      options: [{
        value: "1",
        label: "好用",
        disabled: true
      },
      {
        value: "2",
        label: "方便",
        disabled: false
      },
      {
        value: "3",
        label: "实用",
        disabled: false
      },
      {
        value: "4",
        label: "有效",
        disabled: false
      },
      ],
      props: {
        "size": "default", //多选框组的尺寸，可选值为 large、small、default 或者不设置
      },
    },
    // 单个日期选择
    {
      type: "DatePicker",
      field: "section_day",
      title: "单个日期",
      value: '',
      props: {
        "type": "date",
        "format": "yyyy-MM-dd HH:mm:ss",
        "placeholder": "请选择活动日期",
      }
    },
    //日期范围选择选择
    {
      type: "DatePicker",
      field: "range_day",
      title: "单个日期",
      value: '',
      props: {
        "type": "daterange",
        "format": "yyyy-MM-dd HH:mm:ss",
        "placeholder": "请选择活动日期",
      }
    },
    //单个时间选择
    {  
      type: "TimePicker",
      field: "section_time",
      title: "单个时间",
      value: null, 
      props: {
          "type": "time",
          "placeholder":"请选择获得时间", 
      },
    },
    // 时间范围选择
    {  
      type: "TimePicker",
      field: "range_time",
      title: "时间范围",
      value: null, 
      props: {
          "type": "timerange",
          "placeholder":"请选择获得时间", 
      },
    },
    // 颜色选择
    {
      type: "ColorPicker",
      field: "color",
      title: "颜色",
      value: '#ff7271',  //input值
      props: {
        "alpha": false,  //是否支持透明度选择
        "hue": true, //是否支持色彩选择
        "recommend": false, //是否显示推荐的颜色预设
        "size": "default", //尺寸，可选值为large、small、default或者不设置
        "colors": [], //自定义颜色预设
        "format": "hex", //颜色的格式，可选值为 hsl、hsv、hex、rgb,开启 alpha 时为 rgb，其它为 hex
      },
    },
    // 评分
    {
      type: "rate",
      field: "rate",
      title: "推荐级别",
      value: 3.5,
      props: {
        "count": 10, //star 总数
        "allowHalf": true, //是否允许半选
        "disabled": false, //是否只读，无法进行交互
        "showText": true, //是否显示提示文字
        "clearable": true, //是否可以取消选择
      },
      validate: [{
        required: true,
        type: 'number',
        min: 3,
        message: '请大于3颗星',
        trigger: 'change'
      }]
    },
    //滑块
    {
      type: "slider",
      field: "slider",
      title: "滑块",
      value: [0, 50], //滑块选定的值。普通模式下，数据格式为数字，在双滑块模式下，数据格式为长度是2的数组，且每项都为数字
      props: {
        "min": 0, //最小值
        "max": 100, //最大值
        "step": 1, //步长，取值建议能被（max - min）整除
        "disabled": false, //是否禁用滑块
        "range": true, //是否开启双滑块模式
        "showInput": false, //是否显示数字输入框，仅在单滑块模式下有效
        "showStops": true, //是否显示间断点，建议在 step 不密集时使用
        "showTip": "hover", //提示的显示控制，可选值为 hover（悬停，默认）、always（总是可见）、never（不可见）
        "tipFormat": undefined, //Slider 会把当前值传给 tip-format，并在 Tooltip 中显示 tip-format 的返回值，若为 null，则隐藏 Tooltip
        "inputSize": "small", //数字输入框的尺寸，可选值为large、small、default或者不填，仅在开启 show-input 时有效
      },
    },
    // 上传
    {
      type: "Upload",
      field: "pic",
      title: "轮播图",
      value: [],
      props: {
        "type": "select", //上传控件的类型，可选值为 select（点击选择），drag（支持拖拽）
        "uploadType": "image", //上传文件类型，可选值为 image（图片上传），file（文件上传）
        "action": "", //上传的地址，必填
        "headers": {}, //设置上传的请求头部
        "multiple": true, //是否支持多选文件
        "data": {}, //上传时附带的额外参数
        "name": "", //上传的文件字段名
        "withCredentials": false, //支持发送 cookie 凭证信息
        "accept": "", //接受上传的文件类型
        "format": [], //支持的文件类型，与 accept 不同的是，format 是识别文件的后缀名，accept 为 input 标签原生的 accept 属性，会在选择文件时过滤，可以两者结合使用
        "maxSize": undefined, //文件大小限制，单位 kb
        "maxLength": 4, //上传文件最大数
        "beforeUpload": () => {}, //上传文件之前的钩子，参数为上传的文件，若返回 false 或者 Promise 则停止上传
        "onProgress": () => {}, //文件上传时的钩子，返回字段为 event, file, fileList
        "onSuccess": function(res, file){
          file.url = res.data.file_url
        }, //文件上传成功时的钩子，返回字段为 response, file, fileList, 使用$f.uploadPush(field,filePath) 将上传后的路径添加到value中
        "onError": (error, file, fileList) => {
          console.log(error);
          console.log(file);
          console.log(fileList);
        }, //文件上传失败时的钩子，返回字段为 error, file, fileList
        "onPreview": () => {}, //点击已上传的文件链接时的钩子，返回字段为 file， 可以通过 file.response 拿到服务端返回数据
        "onRemove": () => {}, //文件列表移除文件时的钩子，返回字段为 file, fileList
        "onFormatError": () => {}, //文件格式验证失败时的钩子，返回字段为 file, fileList
        "onExceededSize": () => {}, //文件超出指定大小限制时的钩子，返回字段为 file, fileList
        handleIcon: 'ios-eye-outline', //操作按钮的图标 ,设置为false将不显示
        //点击操作按钮事件
        // onHandle:(src)=>{},
        allowRemove: true,  //是否可删除,设置为false是不显示删除按钮
      },
      validate: [{
        required: true,
        type: 'array',
        min: 3,
        message: '请上传3张图片',
        trigger: 'change'
      }]
    },
    // 富文本组件
    {
      type:'editor',
      field:'editor',
      title:'editor',
      value:'',
      props:{
          init(editor){
              //todo 初始化
          }
      }   
   },
   {
    type: "switch",
    title: "开关",
    field: "switch",
    value: "0",
    props: {
      "trueValue": "1",
      "falseValue": "0",
      "slot": {
        "open": "开启",
        "close": "关闭"
      }
    }
  },
  ],
  api: {
    get: 'http://xiaokang.local.com/route.php?m=admin&mty=sys&a=form&format=json&id='
  }
}

export {
  config
}