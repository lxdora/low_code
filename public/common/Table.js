import request from "@library/api/_request"
const config = {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "user_name",
      "label": "用户名"
    },
    {
      "prop": "role_id",
      "label": "所属角色"
    }
  ],
  "showOperator": true,  // 是否显示操作列
  "api": {
    "get": "http://xiaokang.local.com/route.php?m=admin&mty=sys&format=json"
  },
  "filters": {   // 过滤条件
    rule:  [
      {
        "type": "input",
        "value": "",
        "field": "s_user_name",
        props:{
          placeholder: "用户名",
          clearable: true,
        }
      },
      {
        type: "select",
        title: "",
        field: "s_role_id",
        value: "",
        props: {
          "placeholder": "请选择",
          clearable: true,
        },
        options: [
          {
            label: '管理员',
            value: '1'
          },
          {
            label: '录入人员',
            value: '2'
          },
          {
            label: "审核人员",
            value: '3'
          }
        ],
    },
    ],
  },
  "addBtn": {
    "show": true,
    "label": "新增账号",
    "path": "/edit-account"
  },
  commands: [
    {
      type: "edit",
      label: "编辑",
      fun: function(row){
        let path = 'edit-account'
        this.$router.push({path: path, query:{id: row.id, type: "edit", page: row.page, count: row.count, filterUrl: row.filterUrl}})
      }
    },
    {
      type: "delete",
      label: "删除",
      fun: function(row){
        this.$confirm(`是否确认删除名称为${row.name}的记录`, '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'error'
        }).then(() => {
          request({
            type: 'delete',
            url: '/transcode-server-platform/task',
            data: {
              ids: row.id
            },
            formContentType: 'application/x-www-form-urlencoded'
          }).then(()=>{
            this.$message({
              type: 'success',
              message: '删除成功!'
            });
            this.$store.commit('SET_TASK_REFRESH', true)
          })
        }).catch(() => {
          this.$message({
            type: 'info',
            message: '已取消删除'
          });          
        });          
      }
    }
  ],
}

export {
  config
}