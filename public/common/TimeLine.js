const config = {
  "type": "Timeline",
  "reverse": true,   // 正序还是倒序
  api: {
    get: "http://xiaokang.local.com/logs.php?format=json"
  },
  path: '/log-detail'
}

export {
  config
}