exports.config = {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "title",
      "label": "标题"
    },
    {
      "prop": "catalog_id",
      "label": "一级分类"
    },
    {
      "prop": "catalog2_id",
      "label": "二级分类"
    }
  ],
  "showOperator": true,
  "api": {"get": "http://xiaokang.local.com/route.php?m=publishdata&format=json"},
  "filters": {
    rule:  [
      {
        "type": "select",
        "value": "",
        "field": "s_article_type",
        "props": {
          "placeholder": "内容类型",
          clearable: true,
        },
        "options": [
          {
            "value": "0",
            "label": "图文"
          },
          {
            "value": "1",
            "label": "图集"
          },
          {
            "value": "2",
            "label": "视频"
          },
          {
            "value": "3",
            "label": "PDF文件"
          },
          {
            "value": "4",
            "label": "音频"
          }
        ],
        "api": ""
      },
      {
        "type": "select",
        "value": "",
        "field": "s_status",
        "props": {
          "placeholder": "内容状态",
          clearable: true,
        },
        "options": [
          {
            "value": "1",
            "label": "待审核"
          },
          {
            "value": "2",
            "label": "已审核"
          },
          {
            "value": "3",
            "label": "已上报"
          },
          {
            "value": "4",
            "label": "已打回"
          },
          {
            "value": "5",
            "label": "上报失败"
          }
        ],
        "api": ""
      },
      {
        "type": "input",
        "value": "",
        "field": "s_title",
        props:{
          placeholder: "标题",
          clearable: true,
        }
      },
      {
        type: "select",
        title: "",
        field: "s_catalog_id",
        value: "",
        props: {
          "placeholder": "一级分类",
          clearable: true,
        },
        options: [],
        effect:{
          fetch: {
              action:'http://xiaokang.local.com/route.php?m=catalog&format=json',
              to: 'options',
              method:'GET',
              parse(res){
                const options = {}
                res.data.data.forEach(element => {
                  if(element.parent_id){
                    options[element['parent_id_all']['id']] = element['parent_id_all']['name']
                  }
                });
                const optionArr = []
                for(const key in options){
                  optionArr.push({
                    label: options[key],
                    value: key
                  })
                }
                return optionArr
              }
          }
        }
    },
      {
        type: "select",
        value: "",
        field: "s_is_sensitive",
        props: {
          placeholder: "敏感稿件",
          clearable: true,
        },
        options: [
          {
            "label": "是",
            "value": true
          },
          {
            "label": "否",
            "value": false
          }
        ],
        "api": ""
      },
      {
        type: "select",
        value: "",
        field: "s_is_quality",
        props: {
          placeholder: "精品",
          clearable: true,
        },
        "options": [
          {
            "label": "是",
            "value": true
          },
          {
            "label": "否",
            "value": false
          }
        ],
        "api": ""
      }
    ],
  },
}