exports.config = {
  "type": "menu",
  "data":[
    {
      "id": "0",
      "label": "数据录入",
      "router": "/",
      "name": "DataRecord",
      "meta": [{"label":"数据录入"}],
      "show": true,
      "children": [
        {
          "id": "01",
          "label": "我的草稿",
          "router": "/my-draft",
          "name": "MyDraft",
          "meta": [{"label":"我的草稿"}],
          "show": true,
          "component": "DataRecord/MyDraft"
        },
        {
          "id": "02",
          "label": "我的数据",
          "router": "/my-data",
          "name": "MyData",
          "meta": [{"label":"我的数据"}],
          "show": true,
          "component": "DataRecord/MyData"
        },
        {
          "id": "03",
          "label": "图文数据",
          "router": "/image-text",
          "name": "ImageText",
          "meta": [{"label":"图文数据"}],
          "show": true,
          "component": "DataRecord/ImageText"
        },
        {
          "id": "031",
          "label": "编辑图文数据",
          "router": "/edit-image-text",
          "name": "EditImageText",
          "meta": [{"label":"图文数据", "path": "/image-text"}, {"label": "编辑图文数据"}],
          "show": false,
          "component": "DataRecord/EditImageText"
        },
        {
          "id": "04",
          "label": "图集数据",
          "router": "/image-group",
          "name": "ImageGroup",
          "meta": [{"label":"图集数据"}],
          "show": true,
          "component": "DataRecord/ImageGroup"
        },
        {
          "id": "041",
          "label": "编辑图集数据",
          "router": "/edit-image-group",
          "name": "EditImageGroup",
          "meta": [{"label":"图集数据", "path":"/image-group"}, {"label": "编辑图集数据"}],
          "show": false,
          "component": "DataRecord/EditImageGroup"
        },
        {
          "id": "05",
          "label": "视频数据",
          "router": "/video",
          "name": "Video",
          "meta": [{"label":"视频数据"}],
          "show": true,
          "component":"DataRecord/VideoData"
        },
        {
          "id": "051",
          "label": "编辑视频数据",
          "router": "/edit-video-data",
          "name": "EditVideoData",
          "meta": [{"label":"视频数据", path: '/video'}, {label: "编辑视频数据"}],
          "show": false,
          "component":"DataRecord/EditVideoData"
        },
        {
          "id": "06",
          "label": "音频数据",
          "router": "/audio-data",
          "name": "AudioData",
          "meta": [{"label":"音频数据"}],
          "show": true,
          "component":"DataRecord/AudioData"
        },
        {
          "id": "061",
          "label": "编辑音频数据",
          "router": "/edit-audio-data",
          "name": "EditAudioData",
          "meta": [{"label":"音频数据", "path": "/audio-data"}, {"label": "编辑音频数据"}],
          "show": false,
          "component":"DataRecord/EditAudioData"
        },
        {
          "id": "07",
          "label": "文件数据",
          "router": "/file-data",
          "name": "FileData",
          "meta": [{"label":"文件数据"}],
          "show": true,
          "component":"DataRecord/FileData"
        },
        {
          "id": "08",
          "label": "回收站",
          "router": "/recycle",
          "name": "Recycle",
          "meta": [{"label":"回收站"}],
          "show": true,
          "component":"DataRecord/Recycle"
        }
      ]
    },
    {
      "id": "1",
      "label": "数据审核",
      "meta": [{"label":"数据录入"}],
      "show": true,
      "children": [
        {
          "id": "11",
          "label": "数据审核",
          "meta": [{"label":"数据审核"}],
          "show": true,
          "router":"/data-check",
          "name": "DataCheck",
          "component": "DataCheck/DataCheck"
        },
        {
          "id": "12",
          "label": "已上报数据",
          "meta": [{"label":"已上报数据"}],
          "show": true,
          "router":"/published-data",
          "name": "PublishedData",
          "component": "DataCheck/PublishedData"
        },
        {
          "id": "13",
          "label": "回收站",
          "meta": [{"label":"回收站"}],
          "show": true,
          "router":"/recycle",
          "name": "Recycle",
          "component": "DataCheck/Recycle"
        }
      ]
    },
    {
      "id": "2",
      "label": "配置",
      "meta": [{"label":"配置"}],
      "show": true,
      "children": [
        {
          "id": "21",
          "label": "标签管理",
          "meta": [{"label":"标签管理"}],
          "show": true,
          "router": "/label-manage",
          "name": "LabelManage",
          "component": "Config/LabelManage"

        },
        {
          "id": "22",
          "label": "分类管理",
          "meta": [{"label":"分类管理"}],
          "show": true,
          "router": "/category-manage",
          "name": "CategoryManage",
          "component": "Config/CategoryManage"
        },
        {
          "id": "221",
          "label": "编辑分类",
          "meta": [{"label":"分类管理", "path": "/category-manage"}, {"label": "编辑分类"}],
          "show": false,
          "router": "/edit-category",
          "name": "EditCategory",
          "component": "Config/EditCategory"
        },
        {
          "id": "23",
          "label": "附件管理",
          "meta": [{"label":"附件管理"}],
          "show": true,
          "router": "/attach-manage",
          "name": "AttachManage",
          "component": "Config/AttachManage"
        },
        {
          "id": "231",
          "label": "编辑附件",
          "meta": [{"label":"附件管理", path: "/attach-manage"}, {label: "编辑附件"}],
          "show": false,
          "router": "/edit-attach",
          "name": "EditAttach",
          "component": "Config/EditAttach"
        },
        {
          "id": "24",
          "label": "状态管理",
          "meta": [{"label":"状态管理"}],
          "show": true,
          "router": "/status-manage",
          "name": "StatusManage",
          "component": "Config/StatusManage"
        },
        {
          "id": "21",
          "label": "状态编辑",
          "meta": [{"label":"状态管理", "path": "/status-manage"}, {"label": "状态编辑"}],
          "show": false,
          "router": "/edit-status",
          "name": "EditStatus",
          "component": "Config/EditStatus"
        }
      ]
    },
    {
      "id": "3",
      "label": "系统",
      "meta": [{"label":"系统"}],
      "show": true,
      "children": [
        {
          "id": "31",
          "label": "账号管理",
          "router": "/account-manage",
          "name": "AccountManage",
          "meta": [{"label":"账号管理"}],
          "show": true,
          "component": "System/AccountManage"
        },
        {
          "id": "311",
          "label": "账号编辑",
          "router": "/edit-account",
          "name": "EditAccount",
          "meta": [{"label":"账号管理", "path": "/account-manage"}, {"label": "账号编辑"}],
          "show": false,
          "component": "System/EditAccount"
        },
        {
          "id": "32",
          "label": "角色管理",
          "router": "/role-manage",
          "name": "RoleManage",
          "meta": [{"label":"角色管理"}],
          "show": true,
          "component": "System/RoleManage"
        },
        {
          "id": "321",
          "label": "编辑角色",
          "router": "/edit-role",
          "name": "EditRole",
          "meta": [{"label":"角色管理", "path": "/role-manage"}, {"label": "编辑角色"}],
          "show": false,
          "component": "System/EditRole"
        },
        {
          "id": "33",
          "label": "日志管理",
          "router": "/log-manage",
          "name": "LogManage",
          "meta": [{"label":"日志管理"}],
          "show": true,
          "component":"System/LogManage"
        },
        {
          "id": "331",
          "label": "日志详情",
          "router": "/log-detail",
          "name": "LogDetail",
          "meta": [{"label":"日志管理", path: '/log-manage'}, {label: '日志详情'}],
          "show": false,
          "component":"System/LogDetail"
        }
      ]
    }
  ]
}