exports.config = {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "user_name",
      "label": "用户名"
    },
    {
      "prop": "role_id",
      "label": "所属角色"
    },
    {
      "prop": "appid",
      "label": "开发者Appid"
    }
  ],
  "showOperator": true,
  "api": {
    "get": "http://xiaokang.local.com/route.php?m=admin&mty=sys&format=json"
  },
  "editPath": "/edit-account",
  "filters": {
    rule:  [
      {
        "type": "input",
        "value": "",
        "field": "s_user_name",
        props:{
          placeholder: "用户名",
          clearable: true,
        }
      },
      {
        type: "select",
        title: "",
        field: "s_role_id",
        value: "",
        props: {
          "placeholder": "请选择",
          clearable: true,
        },
        options: [
          {
            label: '管理员',
            value: '1'
          },
          {
            label: '录入人员',
            value: '2'
          },
          {
            label: "审核人员",
            value: '3'
          }
        ],
    },
    ],
  },
  "addBtn": {
    "show": true,
    "label": "新增账号",
    "path": "/edit-account"
  },
  commands: [
    {
      type: "edit",
      label: "编辑",
      fun: function(row){
        let path = 'edit-account'
        this.$router.push({path: path, query:{"id": row.id, "type": "edit"}})
      }
    },
    {
      type: "delete",
      label: "删除",
      fun: function(row){
        const url = 'http://xiaokang.local.com/route.php?m=mydata&format=json&id='+row.id
        this.$http.delete(url)
      }
    }
  ],
}