exports.config = {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "name",
      "label": "角色名称"
    },
    {
      "prop": "is_admin",
      "label": "是否是管理员"
    }
  ],
  "showOperator": true,
  "api": {
    "get": "http://xiaokang.local.com/route.php?m=role&mty=sys&format=json"
  },
  "filters": {
    rule:  [
      {
        "type": "input",
        "value": "",
        "field": "s_role_name",
        props:{
          placeholder: "角色名",
          clearable: true,
        }
      },
      {
        type: "select",
        title: "",
        field: "s_role_id",
        value: "",
        props: {
          "placeholder": "请选择",
          clearable: true,
        },
        options: [
          {
            label: '管理员',
            value: '1'
          },
          {
            label: '录入人员',
            value: '2'
          },
          {
            label: "审核人员",
            value: '3'
          }
        ],
    },
    ],
  },
  "addBtn": {
    "show": true,
    "label": "新增角色",
    "path": "/edit-role"
  },
  commands: [
    {
      type: "edit",
      label: "编辑",
      fun: function(row){
        let path = 'edit-account'
        this.$router.push({path: path, query:{"id": row.id, "type": "edit"}})
      }
    },
    {
      type: "delete",
      label: "删除",
      fun: function(row){
        const url = 'http://xiaokang.local.com/route.php?m=mydata&format=json&id='+row.id
        this.$http.delete(url)
      }
    }
  ],
}