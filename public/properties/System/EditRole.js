exports.config = {
  "type": "form",
  "rule": [
    {
      "type": "input",
      "title": "角色名称",
      "field": "name",
      "value": "",
      "props": {
        "type": "text",
        "clearable": true,
        "autofocus": true,
        "autocomplete": "off",
        "placeholder": "",
        "size": "default",
        "spellcheck": false,
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "",
        "trigger": "blur"
      }
      ]
    },
    {
      "type": "switch",
      "title": "是否管理员",
      "field": "is_admin",
      "value": false,
      "props": {
        "trueValue": true,
        "falseValue": false,
        "slot": {
          "open": "是",
          "close": "否"
        }
      }
    },
    {
      "type":"checkbox",
      "title":"菜单权限",
      "field":"s_menu_prms",
      "value":[],
      "options":[
          {"value":"影响力设置","label":"影响力设置","disabled":false},
          {"value":"周榜","label":"周榜","disabled":false},
          {"value":"月榜","label":"月榜","disabled":false},
          {"value":"季榜","label":"季榜","disabled":false},
          {"value":"年榜","label":"年榜","disabled":false},
          {"value":"回收站","label":"回收站","disabled":false},
          {"value":"数据审核","label":"数据审核","disabled":false},
          {"value":"已上报数据","label":"已上报数据","disabled":false},
          {"value":"文件数据","label":"文件数据","disabled":false},
          {"value":"音频数据","label":"音频数据","disabled":false},
          {"value":"视频数据","label":"视频数据","disabled":false},
          {"value":"图集数据","label":"图集数据","disabled":false},
          {"value":"融媒体数据","label":"融媒体数据","disabled":false},
          {"value":"我的数据","label":"我的数据","disabled":false},
          {"value":"我的草稿","label":"我的草稿","disabled":false}
      ]
    },
    {
      "type":"checkbox",
      "title":"可修改",
      "field":"s_canedit_status",
      "value":[],
      "options":[
          {"value":"待审核","label":"待审核","disabled":false},
          {"value":"已审核","label":"已审核","disabled":false},
          {"value":"已上报","label":"已上报","disabled":false},
          {"value":"已打回","label":"已打回","disabled":false},
          {"value":"上报失败","label":"上报失败","disabled":false}
      ]
    },
    {
      "type":"checkbox",
      "title":"可审核",
      "field":"s_canaudit_status",
      "value":[],
      "options":[
          {"value":"待审核","label":"待审核","disabled":false},
          {"value":"已审核","label":"已审核","disabled":false},
          {"value":"已上报","label":"已上报","disabled":false},
          {"value":"已打回","label":"已打回","disabled":false},
          {"value":"上报失败","label":"上报失败","disabled":false}
      ]
    },
    {
      "type":"checkbox",
      "title":"可删除",
      "field":"s_candel_status",
      "value":[],
      "options":[
          {"value":"待审核","label":"待审核","disabled":false},
          {"value":"已审核","label":"已审核","disabled":false},
          {"value":"已上报","label":"已上报","disabled":false},
          {"value":"已打回","label":"已打回","disabled":false},
          {"value":"上报失败","label":"上报失败","disabled":false}
      ]
    },
    {
      "type": "select",
      "field": "see_range",
      "title": "内容所属范围",
      "value": [],
      "options": [{
        "value": "1",
        "label": "仅限自己",
        "disabled": false
      },
      {
        "value": "2",
        "label": "仅限平台",
        "disabled": false
      }
    ]
    }
  ],
  api: {
    get: 'http://xiaokang.local.com/route.php?m=twdata&a=form&format=json&id='
  }
}