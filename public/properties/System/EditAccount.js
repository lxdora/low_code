exports.config = {
  "type": "form",
  "rule": [
    {
      "type": "input",
      "title": "用户名",
      "field": "user_name",
      "value": "",
      "props": {
        "type": "text",
        "clearable": false,
        "disabled": false,
        "readonly": false,
        "rows": 4, 
        "autosize": false,
        "number": false, 
        "autofocus": false,
        "autocomplete": "off",
        "placeholder": "",
        "size": "default",
        "spellcheck": false,
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "",
        "trigger": "blur"
      }
      ]
    },
    {
      "type": "input",
      "title": "密码",
      "field": "password",
      "value": "",
      "props": {
        "type": "password",
        "clearable": false,
        "disabled": false,
        "readonly": false,
        "rows": 4, 
        "autosize": false,
        "number": false, 
        "autofocus": false,
        "autocomplete": "off",
        "placeholder": "至少包含两种以上字符，不少于8位",
        "size": "default",
        "spellcheck": false,
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "",
        "trigger": "blur"
      }
      ]
    },
    {
      "type": "select",
      "field": "role_id",
      "title": "所属角色",
      "value": [],
      "options": [{
        "value": "1",
        "label": "管理员",
        "disabled": false
      },
      {
        "value": "2",
        "label": "录入人员",
        "disabled": false
      },
      {
        "value": "3",
        "label": "审核人员",
        "disabled": false
      }
      ],
      "props": {
        "placement": "bottom"
      }
    },
    {
      "type": "input",
      "title": "开发者Appid",
      "field": "appid"
    },
    {
      "type": "input",
      "title": "开发者Appkey",
      "field": "appkey"
    },
    {
      "type": "input",
      "title": "最后登录时间",
      "field": "last_time"
    }
  ],
  api: {
    get: 'http://xiaokang.local.com/route.php?m=admin&mty=sys&a=form&format=json&id='
  }
}