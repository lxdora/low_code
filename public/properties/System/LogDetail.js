exports.config = {
  "type": "form",
  "rule": [
    {
      "type": "input",
      "title": "操作前",
      "field": "pre_data",
      "value": "",
      "props": {
        "type": "textarea",
        "rows":10
      }
    },
    {
      "type": "input",
      "title": "操作后",
      "field": "up_data",
      "value": "",
      "props": {
        "type": "textarea",
        "rows":10
      }
    },
  ],
  api: {
    get: 'http://xiaokang.local.com/logs.php?a=detail&format=json&id='
  }
}