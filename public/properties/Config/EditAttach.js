exports.config =  {
  "type": "form",
  "rule": [
    {
      "type": "input",
      "title": "内容ID",
      "field": "content_id",
      "value": "",
    },
    {
      "type": "input",
      "title": "附件标题",
      "field": "title",
      "value": "",
    },
    {
      "type": "input",
      "title": "附件路径",
      "field": "filepath",
      "value": "",
    },
    {
      "type": "input",
      "title": "文件名",
      "field": "filename",
      "value": "",
    },
    {
      "type": "input",
      "title": "文件大小",
      "field": "filesize",
      "value": "",
    },
    {
      "type": "input",
      "title": "文件类型",
      "field": "filetype",
      "value": "",
    },
    {
      "type": "input",
      "title": "图片宽度",
      "field": "imgwidth",
      "value": "",
    },
    {
      "type": "input",
      "title": "图片高度",
      "field": "imgheight",
      "value": "",
    },
  ],
  api: {
    get: 'http://xiaokang.local.com/route.php?m=attach&a=form&format=json&id=',
    post: 'http://xiaokang.local.com/route.php?'
  }
}
