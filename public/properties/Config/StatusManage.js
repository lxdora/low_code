exports.config = {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "name",
      "label": "状态名称"
    },
    {
      "prop": "op",
      "label": "操作名"
    }
  ],
  "showOperator": true,
  "api": {"get": "http://xiaokang.local.com/route.php?m=status&format=json"},
  "editPath": "/edit-status",
  "filters": {
    rule:  [
    {
      "type": "input",
      "value": "",
      "field": "s_name",
      props:{
        placeholder: "状态名称",
        clearable: true,
      }
    }
    ],
  },
  commands: [
    {
      type: "edit",
      label: "编辑",
      fun: function(row){
        let path = 'edit-status'
        this.$router.push({path: path, query:{"id": row.id, "type": "edit"}})
      }
    },
    {
      type: "delete",
      label: "删除",
      fun: function(row){
        const url = 'http://xiaokang.local.com/route.php?m=status&format=json&id='+row.id
        this.$http.delete(url)
      }
    }
  ],
}