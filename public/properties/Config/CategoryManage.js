exports.config =  {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "name",
      "label": "分类名称"
    },
    {
      "prop": "parent_id",
      "label": "父级分类"
    }
  ],
  "showOperator": true,
  "api": {"get": "http://xiaokang.local.com/route.php?m=catalog&format=json"},
  "addBtn": {
    "show": false,
    "label": "",
    "path": ""
  },
  "filters": {
    rule:  [
      {
        type: "select",
        title: "",
        field: "s_parent_id",
        value: "",
        props: {
          "placeholder": "请选择",
          clearable: true,
        },
        options: [],
        effect:{
          fetch: {
              action:'http://xiaokang.local.com/route.php?m=catalog&format=json',
              to: 'options',
              method:'GET',
              parse(res){
                const options = {}
                res.data.data.forEach(element => {
                  if(element.parent_id){
                    options[element['parent_id_all']['id']] = element['parent_id_all']['name']
                  }
                });
                const optionArr = []
                for(const key in options){
                  optionArr.push({
                    label: options[key],
                    value: key
                  })
                }
                return optionArr
              }
          }
        }
    },
    {
      "type": "input",
      "value": "",
      "field": "s_name",
      props:{
        placeholder: "分类名称",
        clearable: true,
      }
    },
    ],
  },
  commands: [
    {
      type: "edit",
      label: "编辑",
      fun: function(row){
        let path = 'edit-category'
        this.$router.push({path: path, query:{"id": row.id, "type": "edit"}})
      }
    },
    {
      type: "delete",
      label: "删除",
      fun: function(row){
        const url = 'http://xiaokang.local.com/route.php?m=mydata&format=json&id='+row.id
        this.$http.delete(url)
      }
    }
  ],
}