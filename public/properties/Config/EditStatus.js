exports.config =  {
  "type": "form",
  "rule": [
    {
      "type": "input",
      "title": "状态名称",
      "field": "name",
      "value": "",
      "props": {
        "type": "text",
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "",
        "trigger": "blur"
      }
      ]
    },
    {
      "type": "ColorPicker",
      "field":"color",
      "title": "标记颜色",
      "value": "#ff7271",
      "props": {
        "alpha": false,
        "hue": true,
        "recommend": false,
        "size": "default",
        "colors": [],
        "format": "hex"
      }
    },
    {
      "type": "input",
      "title": "操作名",
      "field": "op",
      "value": "",
      "props": {
        "type": "text",
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "",
        "trigger": "blur"
      }
      ]
    },
    {
      "type": "checkbox",
      "title": "流程状态",
      "field": "s_next_status",
      "value": [],
      "options": [
        {
          "value": "待审核",
          "label": "待审核"
        },
        {
          "value": "已审核",
          "label": "已审核"
        },
        {
          "value": "已上报",
          "label": "已上报"
        },
        {
          "value": "已打回",
          "label": "已打回"
        },
        {
          "value": "上报失败",
          "label": "上报失败"
        }
      ],
      "props": {
        "size": "default"
      }
    },
    {
      "type": "select",
      "field": "auth_id",
      "title": "转发接口",
      "value": ["104", "105"],
      "props": {
        "multiple": false,
        "clearable": true,
        "filterable": true,
        "size": "default",
        "placeholder": "当到达此状态是将推送到此接口配置",
        "not-found-text": "无匹配数据",
        "placement": "bottom",
        "disabled": false
      },
      "options": [{
        "value": "省小康平台",
        "label": "省小康平台",
        "disabled": false
      }
      ]
    },
    {
      "type": "input",
      "title": "顺序",
      "field": "order_id",
      "value": "",
      "props": {
        "type": "text",
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "",
        "trigger": "blur"
      }
      ]
    }
  ],
  api: {
    get: 'http://xiaokang.local.com/route.php?m=status&a=form&format=json&id='
  }
}
