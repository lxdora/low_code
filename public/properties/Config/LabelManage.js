exports.config = {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "name",
      "label": "标签名称"
    },
    {
      "prop": "parent_id",
      "label": "父级标签"
    }
  ],
  "showOperator": false,
  "api": {"get": "http://xiaokang.local.com/route.php?m=tags&format=json"},
  "filters": {
    rule:  [
      {
        type: "select",
        title: "",
        field: "s_parent_id",
        value: "",
        props: {
          "placeholder": "请选择",
          clearable: true,
        },
        options: [],
        effect:{
          fetch: {
              action:'http://xiaokang.local.com/route.php?m=tags&format=json',
              to: 'options',
              method:'GET',
              parse(res){
                const options = {}
                res.data.data.forEach(element => {
                  if(element.parent_id){
                    options[element['parent_id_all']['id']] = element['parent_id_all']['name']
                  }
                });
                const optionArr = []
                for(const key in options){
                  optionArr.push({
                    label: options[key],
                    value: key
                  })
                }
                return optionArr
              }
          }
        }
    },
    {
      "type": "input",
      "value": "",
      "field": "s_name",
      props:{
        placeholder: "标签名称",
        clearable: true,
      }
    },
    ],
  },
  
}