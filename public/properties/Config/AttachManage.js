exports.config = {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "title",
      "label": "附件标题"
    },
    {
      "prop": "filename",
      "label": "文件名"
    }
  ],
  "showOperator": true,
  "api": {"get": "http://xiaokang.local.com/route.php?m=attach&format=json"},
  "filters": {
    rule:  [
    {
      "type": "input",
      "value": "",
      "field": "s_title",
      props:{
        placeholder: "原始文件名",
        clearable: true,
      }
    },
    {
      "type": "input",
      "value": "",
      "field": "s_filename",
      props:{
        placeholder: "文件名",
        clearable: true,
      }
    },
    ],
  },
  commands: [
    {
      type: "edit",
      label: "编辑",
      fun: function(row){
        let path = 'edit-attach'
        this.$router.push({path: path, query:{"id": row.id, "type": "edit"}})
      }
    },
    {
      type: "delete",
      label: "删除",
      fun: function(row){
        const url = 'http://xiaokang.local.com/route.php?m=mydata&format=json&id='+row.id
        this.$http.delete(url)
      }
    }
  ],
}