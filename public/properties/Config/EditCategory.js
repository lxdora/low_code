exports.config =  {
  "type": "form",
  "rule": [
    {
      "type": "input",
      "title": "分类名称",
      "field": "name",
      "value": "",
      props: {
        "type": "text",
        required: true
      },
      validate: [{
        required: true,
        message: '请输入分类名称',
        trigger: 'blur'
      }],
    },
    {
      type: "input",
      title: "使用字段",
      field: "usefields",
      value: "",
      props: {
        "type": "textarea",
        "rows":10
      }
    }
  ],
  api: {
    get: 'http://xiaokang.local.com/route.php?m=catalog&a=form&format=json&id=',
    post: 'http://xiaokang.local.com/route.php?'
  }
}
