exports.config = {
  "type": "form",
  "rule": [
    {
      "type": "input",
      "title": "标题",
      "field": "title",
      "value": "",
      "props": {
        "type": "text",
        "size": "default",
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "必须填写标题",
        "trigger": "blur"
      }
      ]
    },
    {
      "type": "Upload",
      "field": "file",
      "title": "封面",
      "value": [],
      "props": {
        "type": "drag",
        "uploadType": "image",
        "action": "http://xiaokang.local.com/route.php?a=upload_attach&uploadfield=content&format=json&m=attach&fromm=tjdata&mty=",
        "headers": {},
        "multiple": true,
        "data": {},
        "name": "file",
        "withCredentials": false,
        "accept": "image",
        "format": ["png", "jpg","jpeg","gif"],
        "maxSize": 102400,
        "maxLength": 3,
        "allowRemove": true,
        "emit":["change"],
        "emitPrefix":"upload",
        "onSuccess": function(res, file){
          file.url = 'http://xiaokang.local.com'+res.data.file_url
        }
      },
      "validate": [{
        "required": false,
        "type": "array",
        "max": 3,
        "message": "最多上传3张图片",
        "trigger": "change"
      }]
    },
    {
      "type": "upload",
      "field": "content",
      "title": "视频",
      "value": "",
      "props": {
        "type": "select",
        "uploadType": "file",
        "action": "http://xiaokang.local.com/route.php?a=upload_attach&uploadfield=content&format=json&m=attach&fromm=spdata&mty=",
        "name": "content",
        "multiple": true,
        "accept": "image",
        "format": [".m4a", ".3ga", ".caf", ".mp3", ".vob", ".aac", ".amr"],
        "maxSize": 2048000,
        "maxLength": 1
      }
    },
    {
      "type": "input",
      "title": "视频地址",
      "field": "content_url",
      "value": "",
      "props": {
        "type": "text",
        "size": "default",
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "必须填写视频地址",
        "trigger": "blur"
      }
      ]
    },
   {
    "type": "select",
    "title": "一级分类",
    "field": "catalog_id",
    "value": "",
    "props": {
      "type": "text",
      "size": "default",
      "required": true
    },
    "validate": [{
      "required": true,
      "message": "必须填写一级分类",
      "trigger": "blur"
    }],
    "options": [
      {
        "value": "1",
        "label": "中央领导同志相关资料"
      },
      {
        "value": "2",
        "label": "大事记"
      },
      {
        "value": "3",
        "label": "志书年鉴"
      },
      {
        "value": "4",
        "label": "白皮书"
      },
      {
        "value": "5",
        "label": "理论文章、理论著作"
      },
      {
        "value": "6",
        "label": "新闻报道"
      },
      {
        "value": "7",
        "label": "典型人物"
      },
      {
        "value": "8",
        "label": "课题报告"
      },
      {
        "value": "9",
        "label": "专题片"
      },
      {
        "value": "10",
        "label": "出版物"
      },
      {
        "value": "11",
        "label": "文艺作品"
      },
      {
        "value": "12",
        "label": "电影"
      },
      {
        "value": "13",
        "label": "重大工程项目资料"
      },
      {
        "value": "14",
        "label": "个人作品"
      },
      {
        "value": "15",
        "label": "统计数据"
      }
    ]
  },
  {
  "type": "select",
  "title": "二级分类",
  "field": "catalog2_id",
  "value": "",
  "props": {
    "type": "text",
    "size": "default",
    "required": true
  },
  "validate": [{
    "required": true,
    "message": "必须填写二级分类",
    "trigger": "blur"
  }],
  "options": [
    {
      "label": "重要讲话",
      "value": "16"
    },
    {
      "label": "解读反响",
      "value": "18"
    },
    {
      "label": "评论文章",
      "value": "19"
    },
    {
      "label": "其他",
      "value": "21"
    },
    {
      "label": "考察调研",
      "value": "68"
    },
    {
      "label": "指示批示",
      "value": "74"
    },
    {
      "label": "外事活动",
      "value": "110"
    },
    {
      "label": "图书专著",
      "value": "112"
    },
    {
      "label": "重要会议",
      "value": "115"
    }
  ]
},
{
  "type": "input",
  "title": "描述",
  "field": "description",
  "value": "",
  "props": {
    "type": "textarea",
    "size": "default",
    "required": true,
    "rows":8
  },
  "validate": [{
    "required": true,
    "message": "必须填写描述",
    "trigger": "blur"
  }
]
},
{
  "type": "input",
  "title": "关键词",
  "field": "keyword",
  "value": "",
  "props": {
    "type": "textarea",
    "size": "default",
    "required": true,
    "rows":5
  },
  "validate": [{
    "required": true,
    "message": "必须填写关键词",
    "trigger": "blur"
  }
]
},
{
  "type": "input",
  "title": "来源",
  "field": "source",
  "value": "",
  "props": {
    "type": "text",
    "size": "default",
    "required": true
  },
  "validate": [{
    "required": true,
    "message": "必须填写来源",
    "trigger": "blur"
  }
]
},
{
  "type": "input",
  "title": "来源URL",
  "field": "url",
  "value": "",
  "props": {
    "type": "textarea",
    "size": "default",
    "rows":5
  }
},
{
  "type":"checkbox",
  "title":"标签",
  "field":"show_tags",
  "value":[],
  "options":[
      {"value":"五年规划","label":"五年规划","disabled":false},
      {"value":"地方性法规","label":"地方性法规","disabled":false},
      {"value":"党内政策法规","label":"党内政策法规","disabled":false},
      {"value":"宪法","label":"宪法","disabled":false},
      {"value":"国家法律","label":"国家法律","disabled":false},
      {"value":"行政法规","label":"行政法规","disabled":false},
      {"value":"司法解释","label":"司法解释","disabled":false},
      {"value":"党的领导","label":"党的领导","disabled":false},
      {"value":"人民代表大会制度","label":"人民代表大会制度","disabled":false},
      {"value":"中国共产党领导的多党合作和政治协商制度","label":"中国共产党领导的多党合作和政治协商制度","disabled":false},
      {"value":"民族团结","label":"民族团结","disabled":false},
      {"value":"爱国统一战线","label":"爱国统一战线","disabled":false},
      {"value":"党的群团工作","label":"党的群团工作","disabled":false},
      {"value":"行政体制改革","label":"行政体制改革","disabled":false},
      {"value":"中国道路","label":"中国道路","disabled":false}
  ]
},
{
  "type":"checkbox",
  "title":"民族",
  "field":"show_nation",
  "value":[],
  "options":[
      {"value":"汉族","label":"汉族","disabled":false},
      {"value":"蒙古族","label":"蒙古族","disabled":false},
      {"value":"回族","label":"回族","disabled":false},
      {"value":"藏族","label":"藏族","disabled":false},
      {"value":"维吾尔族","label":"维吾尔族","disabled":false},
      {"value":"苗族","label":"苗族","disabled":false},
      {"value":"彝族","label":"彝族","disabled":false},
      {"value":"壮族","label":"壮族","disabled":false},
      {"value":"布依族","label":"布依族","disabled":false},
      {"value":"朝鲜族","label":"朝鲜族","disabled":false},
      {"value":"满族","label":"满族","disabled":false},
      {"value":"侗族","label":"侗族","disabled":false},
      {"value":"瑶族","label":"瑶族","disabled":false},
      {"value":"白族","label":"白族","disabled":false},
      {"value":"土家族","label":"土家族","disabled":false}
  ]
},
{
  "type": "select",
  "title": "领导人",
  "field": "leader",
  "value": "",
  "props": {
    "type": "text",
    "size": "default",
    "required": true
  },
  "validate": [{
    "required": true,
    "message": "必须选择领导人",
    "trigger": "blur"
  }],
  "options": [
    {
      "label": "毛泽东",
      "value": "毛泽东"
    },
    {
      "label": "邓小平",
      "value": "邓小平"
    },
    {
      "label": "江泽民",
      "value": "江泽民"
    },
    {
      "label": "胡锦涛",
      "value": "胡锦涛"
    },
    {
      "label": "习近平",
      "value": "习近平"
    },
    {
      "label": "其他",
      "value": "其他"
    }
  ]
},
{
  "type": "input",
  "title": "会议/讲话名称",
  "field": "conference_name",
  "value": ""
},
{
  "type": "input",
  "title": "主题",
  "field": "theme",
  "value": "",
  "props": {
    "type": "text",
    "size": "default",
    "required": true
  },
  "validate": [{
    "required": true,
    "message": "必须选择领导人",
    "trigger": "blur"
  }]
},
{
  "type": "DatePicker",
  "field": "publish_time",
  "title": "发布时间",
  "value": "",
  "props": {
    "type": "date",
    "format": "yyyy-MM-dd HH:mm:ss",
    "placeholder": "请选择发布时间",
    "required": true
  },
  "validate": [{
    "required": true,
    "message": "必须选择发布时间",
    "trigger": "blur"
  }]
},
{
  "type": "DatePicker",
  "field": "story_time",
  "title": "事件时间",
  "value": "",
  "props": {
    "type": "date",
    "format": "yyyy-MM-dd HH:mm:ss",
    "placeholder": "请选择时间"
  }
},
{
  "type": "switch",
  "title": "是否敏感稿件",
  "field": "is_sensitive",
  "value": "0",
  "props": {
    "trueValue": "1",
    "falseValue": "0",
    "slot": {
      "open": "开启",
      "close": "关闭"
    }
  }
},
{
  "type": "switch",
  "title": "是否精品",
  "field": "is_quality",
  "value": "0",
  "props": {
    "trueValue": "1",
    "falseValue": "0",
    "slot": {
      "open": "开启",
      "close": "关闭"
    }
  }
},
{
  "type": "input",
  "title": "备注",
  "field": "comments",
  "value": "",
  "props": {
    "type": "textarea",
    "size": "default",
    "rows":5
  }
}
],
  api: {
    get: 'http://xiaokang.local.com/route.php?m=twdata&a=form&format=json&id=',
    post: 'http://xiaokang.local.com/route.php'
  }
}