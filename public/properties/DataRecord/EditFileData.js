exports.config = {
  "type": "form",
  "rule": [
    {
      "type": "input",
      "title": "标题",
      "field": "title",
      "value": "",
      "props": {
        "type": "text",
        "size": "default",
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "必须填写标题",
        "trigger": "blur"
      }
      ]
    },
    {
      "type": "Upload",
      "field": "file",
      "title": "封面",
      "value": [],
      "props": {
        "type": "drag",
        "uploadType": "image",
        "action": "http://xiaokang.local.com/route.php?a=upload_attach&uploadfield=content&format=json&m=attach&fromm=tjdata&mty=",
        "headers": {},
        "multiple": true,
        "data": {},
        "name": "file",
        "withCredentials": false,
        "accept": "image",
        "format": ["png", "jpg","jpeg","gif"],
        "maxSize": 102400,
        "maxLength": 3,
        "allowRemove": true,
        "emit":["change"],
        "emitPrefix":"upload",
        "onSuccess": function(res, file){
          file.url = 'http://xiaokang.local.com'+res.data.file_url
        }
      },
      "validate": [{
        "required": false,
        "type": "array",
        "max": 3,
        "message": "最多上传3张图片",
        "trigger": "change"
      }]
    },
    {
      "type": "upload",
      "field": "content",
      "title": "PDF文件",
      "value": "",
      "props": {
        "type": "upload",
        "uploadType": "file",
        "action": "/route.php?a=upload_attach&uploadfield=content&format=json&m=attach&fromm=wjdata&mty=",
        "name": "file",
        "multiple": false,
        "accept": "image",
        "format": [".pdf", ".txt", ".zip", ".docx", ".doc", ".pptx", ".ppt", ".xls", ".xlsx", ".png", ".jpg", ".jpeg", ".gif"],
        "maxSize": 2048000,
        "maxLength": 1
      }
    },
    {
      "type": "input",
      "title": "PDF文件地址",
      "field": "content_url",
      "value": "",
      "props": {
        "type": "text",
        "size": "default",
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "必须填写PDF文件地址",
        "trigger": "blur"
      }
      ]
    },
    {
      "type": "select",
      "title": "一级分类",
      "field": "catalog_id",
      "value": "",
      "props": {
        "type": "text",
        "size": "default",
        "required": true
      },
      "validate": [{
        "required": true,
        "message": "必须填写一级分类",
        "trigger": "blur"
      }],
      options: [],
      effect:{
        fetch: {
            action:'http://xiaokang.local.com/route.php?m=catalog&format=json',
            to: 'options',
            method:'GET',
            parse(res){
              const options = new Set()
              res.data.data.forEach(element => {
                if(element.parent_id){
                  options.add(element.parent_id)
                }
              });
              return Array.from(options).map(row=>{
                  return {
                      label: row,
                      value: row
                  }
              })
            }
        }
      },
      update(val, rule, fapi){
        const axios = require('axios')
        const url = 'http://xiaokang.local.com/route.php?m=catalog&format=json&catalog_id=' + val
        axios.get(url).then(res=>{
          fapi.rule[5].options = res.data.data.data.map(row=>{
            return {
                label: row.name,
                value: row.name
            }
        })
        })
      }
  },
    {
    "type": "select",
    "title": "二级分类",
    "field": "catalog2_id",
    "value": "",
    "props": {
      "type": "text",
      "size": "default",
      "required": true
    },
    "validate": [{
      "required": true,
      "message": "必须填写二级分类",
      "trigger": "blur"
    }],
    "options": [],
  },
{
  "type": "input",
  "title": "描述",
  "field": "content_description",
  "value": "",
  "props": {
    "type": "textarea",
    "size": "default",
    "required": true,
    "rows":8
  },
  "validate": [{
    "required": true,
    "message": "必须填写描述",
    "trigger": "blur"
  }
]
},
{
  "type": "input",
  "title": "关键词",
  "field": "keyword",
  "value": "",
  "props": {
    "type": "textarea",
    "size": "default",
    "required": true,
    "rows":5
  },
  "validate": [{
    "required": true,
    "message": "必须填写关键词",
    "trigger": "blur"
  }
]
},
{
  "type": "input",
  "title": "来源",
  "field": "source",
  "value": "",
  "props": {
    "type": "text",
    "size": "default",
    "required": true
  },
  "validate": [{
    "required": true,
    "message": "必须填写来源",
    "trigger": "blur"
  }
]
},
{
  "type": "input",
  "title": "来源URL",
  "field": "url",
  "value": "",
  "props": {
    "type": "textarea",
    "size": "default",
    "rows":5
  }
},
{
  "type":"checkbox",
  "title":"标签",
  "field":"show_tags",
  "value":[],
  "options":[
  ],
  effect:{
    fetch: {
        action:'http://xiaokang.local.com/route.php?m=tags&format=json',
        to: 'options',
        method:'GET',
        parse(res){
            return res.data.data.map(row=>{
                return {
                    label: row.name,
                    value: row.name
                }
            })
        }
    }
  }
},
{
  "type":"checkbox",
  "title":"民族",
  "field":"show_nation",
  "value":[],
  "options":[
      {"value":"汉族","label":"汉族","disabled":false},
      {"value":"蒙古族","label":"蒙古族","disabled":false},
      {"value":"回族","label":"回族","disabled":false},
      {"value":"藏族","label":"藏族","disabled":false},
      {"value":"维吾尔族","label":"维吾尔族","disabled":false},
      {"value":"苗族","label":"苗族","disabled":false},
      {"value":"彝族","label":"彝族","disabled":false},
      {"value":"壮族","label":"壮族","disabled":false},
      {"value":"布依族","label":"布依族","disabled":false},
      {"value":"朝鲜族","label":"朝鲜族","disabled":false},
      {"value":"满族","label":"满族","disabled":false},
      {"value":"侗族","label":"侗族","disabled":false},
      {"value":"瑶族","label":"瑶族","disabled":false},
      {"value":"白族","label":"白族","disabled":false},
      {"value":"土家族","label":"土家族","disabled":false}
  ]
},
{
  "type": "select",
  "title": "领导人",
  "field": "leader",
  "value": "",
  "props": {
    "type": "text",
    "size": "default",
    "required": true
  },
  "validate": [{
    "required": true,
    "message": "必须选择领导人",
    "trigger": "blur"
  }],
  "options": [
    {
      "label": "毛泽东",
      "value": "毛泽东"
    },
    {
      "label": "邓小平",
      "value": "邓小平"
    },
    {
      "label": "江泽民",
      "value": "江泽民"
    },
    {
      "label": "胡锦涛",
      "value": "胡锦涛"
    },
    {
      "label": "习近平",
      "value": "习近平"
    },
    {
      "label": "其他",
      "value": "其他"
    }
  ]
},
{
  "type": "input",
  "title": "会议/讲话名称",
  "field": "conference_name",
  "value": ""
},
{
  "type": "input",
  "title": "主题",
  "field": "theme",
  "value": "",
  "props": {
    "type": "text",
    "size": "default",
    "required": true
  },
  "validate": [{
    "required": true,
    "message": "必须选择领导人",
    "trigger": "blur"
  }]
},
{
  "type": "DatePicker",
  "field": "publish_time",
  "title": "发布时间",
  "value": "",
  "props": {
    "type": "date",
    "format": "yyyy-MM-dd HH:mm:ss",
    "placeholder": "请选择发布时间",
    "required": true
  },
  "validate": [{
    "required": true,
    "message": "必须选择发布时间",
    "trigger": "blur"
  }]
},
{
  "type": "DatePicker",
  "field": "story_time",
  "title": "事件时间",
  "value": "",
  "props": {
    "type": "date",
    "format": "yyyy-MM-dd HH:mm:ss",
    "placeholder": "请选择时间"
  }
},
{
  "type": "switch",
  "title": "是否敏感稿件",
  "field": "is_sensitive",
  "value": "0",
  "props": {
    "trueValue": "1",
    "falseValue": "0",
    "slot": {
      "open": "开启",
      "close": "关闭"
    }
  }
},
{
  "type": "switch",
  "title": "是否精品",
  "field": "is_quality",
  "value": "0",
  "props": {
    "trueValue": "1",
    "falseValue": "0",
    "slot": {
      "open": "开启",
      "close": "关闭"
    }
  }
},
{
  "type": "input",
  "title": "备注",
  "field": "comments",
  "value": "",
  "props": {
    "type": "textarea",
    "size": "default",
    "rows":5
  }
}
],
  api: {
    get: 'http://xiaokang.local.com/route.php?m=twdata&a=form&format=json&id='
  }
}