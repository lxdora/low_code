exports.config = {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "title",
      "label": "标题"
    },
    {
      "prop": "catalog_id",
      "label": "一级分类"
    },
    {
      "prop": "catalog2_id",
      "label": "二级分类"
    }
  ],
  "showOperator": true,
  "api": {
    "get": "http://xiaokang.local.com/route.php?m=mydraft&format=json"
  },
  commands: [
    {
      type: "edit",
      label: "编辑",
      fun: function(row){
        let path = ''
        switch(row.article_type_all.name){
          case '图文':
            path = 'edit-image-text'
            break
          case '图集':
            path = 'edit-image-group'
            break
          case '音频':
            path = 'edit-audio-data'
            break
          case '视频':
            path = 'edit-video-data'
            break
        }
        this.$router.push({path: path, query:{"id": row.id, "type": "edit"}})
      }
    },
    {
      type: "delete",
      label: "删除",
      fun: function(row){
        const url = 'http://xiaokang.local.com/route.php?a=delete&m=mydraft&mty=&format=json&id='+row.id
        this.$http.post(url).then(()=>{
          location.reload()
        })
      }
    }
  ],
  "filters": {
    rule:  [
      {
        "type": "input",
        "value": "",
        "field": "s_title",
        props:{
          placeholder: "标题",
          clearable: true,
        }
      },
      {
        type: "select",
        title: "",
        field: "s_catalog_id",
        value: "",
        props: {
          "placeholder": "一级分类",
          clearable: true,
        },
        options: [],
        effect:{
          fetch: {
              action:'http://xiaokang.local.com/route.php?m=catalog&format=json',
              to: 'options',
              method:'GET',
              parse(res){
                const options = {}
                res.data.data.forEach(element => {
                  if(element.parent_id){
                    options[element['parent_id_all']['id']] = element['parent_id_all']['name']
                  }
                });
                const optionArr = []
                for(const key in options){
                  optionArr.push({
                    label: options[key],
                    value: key
                  })
                }
                return optionArr
              }
          }
        }
    }
    ],
  },
  "addBtn": {
    "show": false,
    "label": "",
    "path": ""
  }
}