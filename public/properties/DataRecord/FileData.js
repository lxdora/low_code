exports.config = {
  "type": "table",
  "columns": [
    {
      "prop": "id",
      "label": "ID",
      "sortable": true
    },
    {
      "prop": "title",
      "label": "标题"
    },
    {
      "prop": "catalog_id",
      "label": "分类"
    },
    {
      "prop": "status",
      "label": "状态"
    }
  ],
  "showOperator": true,
  "api": {"get": "http://xiaokang.local.com/route.php?m=wjdata&format=json"},
  "filters": {
    rule:   [
      {
        "type": "select",
        "value": "",
        "field": "s_status",
        props:{
          "placeholder": "内容状态",
        },
        "options": [
          {
            "value": "1",
            "label": "待审核"
          },
          {
            "value": "2",
            "label": "已审核"
          },
          {
            "value": "3",
            "label": "已上报"
          },
          {
            "value": "4",
            "label": "已打回"
          },
          {
            "value": "5",
            "label": "上报失败"
          }
        ],
        "api": ""
      },
      {
        "type": "input",
        "value": "",
        "field": "s_title",
        props: {
          "placeholder": "在这里输入标题",
        }
      },
      {
        type: "select",
        title: "",
        field: "s_catalog_id",
        value: "",
        props: {
          "placeholder": "一级分类",
          clearable: true,
        },
        options: [],
        effect:{
          fetch: {
              action:'http://xiaokang.local.com/route.php?m=catalog&format=json',
              to: 'options',
              method:'GET',
              parse(res){
                const options = {}
                res.data.data.forEach(element => {
                  if(element.parent_id){
                    options[element['parent_id_all']['id']] = element['parent_id_all']['name']
                  }
                });
                const optionArr = []
                for(const key in options){
                  optionArr.push({
                    label: options[key],
                    value: key
                  })
                }
                return optionArr
              }
          }
        }
    },
      {
        "type": "select",
        "value": "",
        "field": "s_is_sensitive",
        props:{
          "placeholder": "敏感稿件",
        },
        "options": [
          {
            "label": "是",
            "value": true
          },
          {
            "label": "否",
            "value": false
          }
        ],
        "api": ""
      },
      {
        "type": "select",
        "value": "",
        "field": "s_is_quality",
        props: {
          "placeholder": "精品",
        },
        "options": [
          {
            "label": "是",
            "value": true
          },
          {
            "label": "否",
            "value": false
          }
        ],
        "api": ""
      }
    ]
  },
  commands: [
    {
      type: "edit",
      label: "编辑",
      fun: function(row){
        let path = ''
        switch(row.article_type_all.name){
          case '图文':
            path = 'edit-image-text'
            break
          case '图集':
            path = 'edit-image-group'
            break
          case '音频':
            path = 'edit-audio-data'
            break
          case '视频':
            path = 'edit-video-data'
            break
        }
        this.$router.push({path: path, query:{"id": row.id, "type": "edit"}})
      }
    },
    {
      type: "delete",
      label: "删除",
      fun: function(row){
        const url = 'http://xiaokang.local.com/route.php?a=delete&m=wjdata&mty=&format=json&id='+row.id
        this.$http.post(url).then(()=>{
          location.reload()
        })
      }
    }
  ],
  
}