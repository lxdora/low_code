const API = {
  getConfiguration: '/properties/', // 获取应用列表 
  data: '/route.php',
  log: '/logs.php'
}

export default API;