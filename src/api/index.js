import request from '../http/request'
import Axios from "axios"
import API from "./interface"

function getConfiguration(configFile){
  return Axios.get(API.getConfiguration + configFile)
}

function getData(params) {
  return request({
    url: `${API.data}?m=${params}&format=json`,
    type: 'GET'
  })
}

function addData(data) {
  return request({
    url: API.data,
    type: "POST",
    data
  })
}

function deleteData(params, id){
  return request({
    url: `${API.data}?a=delete&m=${params}&id=${id}`,
    type: "POST"
  })
}

function getLog() {
  return request({
    url: API.log + '?format=json',
    type: "GET"
  })
}



export {
  getConfiguration,
  getData,
  deleteData,
  addData,
  getLog
}